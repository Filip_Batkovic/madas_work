import gevent
import time

import sys
from os.path import dirname
sys.path.append(dirname(sys.path[0]))

from madas_lib import endpoints
from madas_lib.joinable import Joinable
from madas_lib.bidir_connection import RawClient
from madas_lib.mpu_subscriber import MpuSubscriber
from madas_lib.mpu_data import Mpu6050Data, GyroData, AccelData


class DerailDetectionController(Joinable):
    def __init__(self, remote):
        super().__init__()
        self.mpu_subscriber = MpuSubscriber(remote)
        self._register_to_join(self.mpu_subscriber)
        self._register_to_join(gevent.spawn(self.train_handler))
        self.motor_controller = RawClient(endpoints.train_motor_control_client_endpoint(remote), 1, 10)

    def train_handler(self):
        while not self._shutdown_requested():
            mpu_data = self.mpu_subscriber.get_last_recveived_value()
            if mpu_data:
                print(mpu_data.accel.x)
                if -5 < mpu_data.accel.x < 5:
                    self.motor_controller.request(10)
                else:
                    self.motor_controller.request(0)
            gevent.sleep(.2)


if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--remote", "-r", help="The hostname to connect to.", default="localhost")
    args = parser.parse_args()

    print("Starting at {}".format(args.remote))
    m = DerailDetectionController(args.remote)
    gevent.joinall(list(m.to_join()))

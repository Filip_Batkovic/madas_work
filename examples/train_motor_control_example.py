# -*- coding: utf-8 -*-
"""
Test TrainMotorControl without any network connectivity: simply set a few
different speeds.
"""
import sys
from os.path import dirname
sys.path.append(dirname(sys.path[0]))

from madas_lib import train_motor_control


if __name__=="__main__":
    import time
    tmc = train_motor_control.TrainMotorControl()

    for i in (10, 20, -20, 0, 100):
        try:
            tmc.set_speed(i)
            time.sleep(1)
        except train_motor_control.MaxSpeedExceeded:
            pass

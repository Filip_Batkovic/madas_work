import time

import sys
from os.path import dirname
sys.path.append(dirname(sys.path[0]))

from madas_lib.train_motor_control_client import TrainMotorControlClient


if __name__=="__main__":
    t = TrainMotorControlClient("localhost")
    for i in range(-4, 10):
        print(i*10)
        for _ in range(10):
            t.set_speed(i*10)
            time.sleep(.1)

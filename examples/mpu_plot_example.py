import gevent
import matplotlib.pyplot as plt
from collections import deque

import sys
from os.path import dirname
sys.path.append(dirname(sys.path[0]))

from madas_lib import endpoints
from madas_lib.mpu_subscriber import MpuSubscriber
from madas_lib.joinable import Joinable
from madas_lib.mpu_data import Mpu6050Data, GyroData, AccelData


class FixedElementQueue(deque):
    """A queue that will store an exact number of values: whenever a new value
    is added, the oldest one will be removed.

    Older values are stored at lower indices.
    """
    def __init__(self, number, init):
        """Initialize the queue; it will always hold number elements that will
        be initialized to init."""
        super().__init__([init]*number)

    def append_and_pop(self, value):
        """Append a new element and remove the oldest one."""
        self.append(value)
        self.popleft()


class MpuPlotClient(Joinable):
    def __init__(self, remote):
        super().__init__()
        self.subscriber = MpuSubscriber(remote)
        self._register_to_join(self.subscriber)
        self._register_to_join(gevent.spawn(self.gyro_handler, 20, .2))

    def __plot_new_values(self, fig, x, y, z):
        """Clear the previous plot (if any) and plot the x, y and z values."""
        plt.clf()
        for i, s in ((x, 'x'), (y, 'y'), (z, 'z')):
            plt.plot(range(len(i)), i, label=s)
        plt.xlabel('time [s]')
        plt.ylabel('accelleration [m/s^2]')
        plt.legend()
        fig.canvas.draw()

    def gyro_handler(self, history_length, update_time):
        """Run an endless loop to receive GyroData every update_time seconds, store
        the last history_length values, and show those in a plot."""
        data = FixedElementQueue(history_length, GyroData(0, 0, 0))
        fig = plt.gcf()
        fig.show()

        while not self._shutdown_requested():
            rcvd = self.subscriber.get_last_recveived_value()
            if rcvd:
                data.append_and_pop(rcvd.accel)
                self.__plot_new_values(fig,
                        [i.x for i in data],
                        [i.y for i in data],
                        [i.z for i in data])
            gevent.sleep(.2)


if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--remote", "-r", help="The hostname to connect to.", default="localhost")
    args = parser.parse_args()

    print("Starting at {}".format(args.remote))
    m = MpuPlotClient(args.remote)
    gevent.sleep(10)
    m.request_shutdown()
    gevent.joinall(list(m.to_join()))


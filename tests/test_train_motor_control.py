import unittest
from unittest.mock import patch, call, Mock

import sys
from os.path import dirname
sys.path.append(dirname(sys.path[0]))

with patch.dict('sys.modules', RPi=Mock()):
    import madas_lib.train_motor_control as train_motor_control
    gpio = sys.modules["RPi"].GPIO


class TrainMotorControlTest(unittest.TestCase):
    def test_init(self):
        dut = train_motor_control.TrainMotorControl()

        gpio.setmode.assert_called_once_with(gpio.BOARD)
        # pins 16, 18, and 22 should be outputs
        gpio.setup.assert_has_calls(
                [
                    call(16, gpio.OUT),
                    call(18, gpio.OUT),
                    call(22, gpio.OUT)
                ],
                True
            )
        # pins 16 and 18 need to be low/high, respectively
        gpio.output.assert_has_calls(
                [
                    call(16, gpio.LOW),
                    call(18, gpio.HIGH),
                ],
                True
            )
        # there needs to be a 2KHz PWM on pin 22
        gpio.PWM.assert_called_once_with(22, 2000)

    def test_pwm(self):
        dut = train_motor_control.TrainMotorControl()
        pwm = gpio.PWM()

        # only forward, for now
        for speed in (0, 10, 7, 15, 0, 49, 50, 48, 1):
            # reset the mocks so that we're only tracing what set_speed is
            # doing
            pwm.reset_mock()
            gpio.output.reset_mock()

            dut.set_speed(speed)

            pwm.ChangeDutyCycle.assert_called_once_with(speed)
            gpio.output.assert_called_once_with(18, gpio.HIGH)

        # now, go bi-directional
        for speed in (0, -10, 7, -7, -15, -0, 0, -50, 50, 1, -1):
            # reset the mocks so that we're only tracing what set_speed is
            # doing
            pwm.reset_mock()
            gpio.output.reset_mock()

            dut.set_speed(speed)

            pwm.ChangeDutyCycle.assert_called_once_with(abs(speed))
            gpio.output.assert_called_once_with(18, gpio.HIGH if speed >= 0 else gpio.LOW)

    def test_max_speed(self):
        dut = train_motor_control.TrainMotorControl()
        pwm = gpio.PWM()

        for speed in (0, -50, 51, -1000, -49, -51, 51, 50, -50, -1):
            # reset the mocks so that we're only tracing what set_speed is
            # doing
            pwm.reset_mock()
            gpio.output.reset_mock()
            if 0 <= abs(speed) <= 50:
                dut.set_speed(speed)
                pwm.ChangeDutyCycle.assert_called_once_with(abs(speed))
                gpio.output.assert_called_once_with(18, gpio.HIGH if speed >= 0 else gpio.LOW)
            elif 50 < abs(speed) <= 100:
                with self.assertRaises(train_motor_control.MaxSpeedExceeded):
                    dut.set_speed(speed)
                    pwm.ChangeDutyCycle.assert_not_called()
                    gpio.output.assert_not_called()
            else:
                with self.assertRaises(ValueError):
                    dut.set_speed(speed)
                    pwm.ChangeDutyCycle.assert_not_called()
                    gpio.output.assert_not_called()


if __name__ == "__main__":
    unittest.main()


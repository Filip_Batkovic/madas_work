import sys
from os.path import dirname
sys.path.append(dirname(sys.path[0]))

import unittest
from madas_lib import joinable

class TestJoinable(unittest.TestCase):
    def test_joinables(self):
        j = joinable.Joinable()
        j._register_to_join(1)
        j._register_to_join(2)

        self.assertEqual(set(j.to_join()), set([1, 2]))

    def test_sub_joinables(self):
        # j            1, 2
        # |- sj1       10, 11
        # |   |- sj3   100
        # |- sj2       20

        sj3 = joinable.Joinable()
        sj3._register_to_join(100)

        sj1 = joinable.Joinable()
        sj1._register_to_join(10)
        sj1._register_to_join(11)
        sj1._register_to_join(sj3)

        sj2 = joinable.Joinable()
        sj2._register_to_join(20)

        j = joinable.Joinable()
        j._register_to_join(1)
        j._register_to_join(2)
        j._register_to_join(sj1)
        j._register_to_join(sj2)

        self.assertEqual(set(j.to_join()), set([10, 11, 100, 20, 1, 2]))

    def test_shutdown(self):
        sj3 = joinable.Joinable()

        sj1 = joinable.Joinable()
        sj1._register_to_join(sj3)

        sj2 = joinable.Joinable()

        j = joinable.Joinable()
        j._register_to_join(sj1)
        j._register_to_join(sj2)

        j.request_shutdown()

        assert(j._shutdown_requested())
        assert(sj1._shutdown_requested())
        assert(sj2._shutdown_requested())
        assert(sj3._shutdown_requested())


if __name__ == "__main__":
    unittest.main()

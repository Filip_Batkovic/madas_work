from madas_lib.subscriber import Subscriber
from madas_lib import endpoints

class MpuSubscriber(Subscriber):
    """Encapsulates a Subscriber especially for the Mpu6050."""
    def __init__(self, remote):
        super().__init__(endpoints.mpu6050_subscriber_endpoint(remote), 2)

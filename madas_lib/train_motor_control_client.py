from madas_lib.bidir_connection import RawClient
from madas_lib import endpoints


class TrainMotorControlClient:
    def __init__(self, remote):
        self.__client = RawClient(endpoints.train_motor_control_client_endpoint(remote), 1, 10)

    def set_speed(self, value):
        self.__client.request(value)

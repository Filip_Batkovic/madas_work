import gevent
import zmq.green as zmq

from madas_lib.joinable import Joinable


class RawPublisher:
    """This class creates a zmq.PUB socket that transmits a value on every call
    to send() to the defined endpoint. Note that, for most cases, you'll want
    to use a Publisher instead of this class: the Publisher additionally
    contains a loop to send the data.
    """
    def __init__(self, endpoint):
        context = zmq.Context()
        self.__socket = context.socket(zmq.PUB)
        self.__socket.bind(endpoint)

    def send(self, value):
        """Send the given value."""
        self.__socket.send_pyobj(value)


class Publisher(RawPublisher, Joinable):
    """This is a base class to implement Publishers: it will call sensor_func
    every interval seconds and publish the returned data on the given
    endpoint.
    """
    def __init__(self, sensor_func, endpoint, interval):
        RawPublisher.__init__(self, endpoint)
        Joinable.__init__(self)
        self.__sensor_func = sensor_func
        self.__interval = interval
        self._register_to_join(gevent.spawn(self.__serve_sensor_data))

    def __serve_sensor_data(self):
        while not self._shutdown_requested():
            self.send(self.__sensor_func())
            gevent.sleep(self.__interval)

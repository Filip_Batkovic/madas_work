import gevent
import zmq.green as zmq
from madas_lib.exceptions import ConnectionLost
from madas_lib.joinable import Joinable


class RawSubscriber:
    def __init__(self, endpoint, timeout, callback=None):
        """Initialize a Subscriber object. This will create a greenlet to
        receive data from a zmq.SUB socket that will automatically connect to
        endpoint.  If timeout seconds or more passes between two received
        packets, a ConnectionLost exeception is raised.

        If callback is given, it needs to be a callable taking exactly one
        parameter of the type(s) that will be received from the socket.
        callback will be called for each received value.
        """
        context = zmq.Context()
        self.__sub_socket = context.socket(zmq.SUB)
        self.__sub_socket.setsockopt_string(zmq.SUBSCRIBE, '')
        self.__sub_socket.connect(endpoint)

        self.__endpoint = endpoint
        self.__timeout = timeout

        self.__value = None
        self.__callback = callback

        self.__poller = zmq.Poller()
        self.__poller.register(self.__sub_socket, zmq.POLLIN)

    def get_last_recveived_value(self):
        """Returns the last received value, or None if no value has been
        received yet.
        """
        return self.__value

    def recv(self):
        socks = dict(self.__poller.poll(self.__timeout * 1000))
        if self.__sub_socket in socks and socks[self.__sub_socket] == zmq.POLLIN:
            self.__value = self.__sub_socket.recv_pyobj()
            if self.__callback:
                self.__callback(self.__value)
        else:
            raise ConnectionLost(self.__endpoint)


class Subscriber(RawSubscriber, Joinable):
    def __init__(self, endpoint, timeout, callback=None):
        """Initialize a Subscriber object. This will create a greenlet to
        receive data from a zmq.SUB socket that will automatically connect to
        endpoint.  If timeout seconds or more passes between two received
        packets, a ConnectionLost exeception is raised.

        If callback is given, it needs to be a callable taking exactly one
        parameter of the type(s) that will be received from the socket.
        callback will be called for each received value.
        """
        RawSubscriber.__init__(self, endpoint, timeout, callback)
        Joinable.__init__(self)

        self.__greenlet = gevent.spawn(self.__recv)
        self._register_to_join(self.__greenlet)

    def get_last_recveived_value(self):
        """Returns the last received value, or None if no value has been
        received yet.
        """
        if self.__greenlet:
            return super().get_last_recveived_value()
        else:
            raise ConnectionLost(self.__endpoint)

    def __recv(self):
        while not self._shutdown_requested():
            self.recv()

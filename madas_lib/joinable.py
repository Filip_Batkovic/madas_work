import itertools


class Joinable:
    """Implements a mixin that deals with greenlets that need to be joined
    somewhere down the line. Using this mixin enables registering greenlets or
    other Joinables, all of which will be recursively collected with to_join().

    Additionally, it implements a shutdown mechanism: use while not
    _shotdown_requested() to implement an 'endless' loop that can be cancelled
    by calling request_shutdown()."""
    def __init__(self):
        self.__shutdown = False
        self.__greenlets = []
        self.__joinables = []

    def _register_to_join(self, x):
        if isinstance(x, Joinable):
            self.__joinables.append(x)
        else:
            self.__greenlets.append(x)

    def to_join(self):
        to_join_sub = [j.to_join() for j in self.__joinables]
        return itertools.chain(self.__greenlets, *to_join_sub)

    def _shutdown_requested(self):
        return self.__shutdown

    def request_shutdown(self):
        for j in self.__joinables:
            j.request_shutdown()
        self.__shutdown = True

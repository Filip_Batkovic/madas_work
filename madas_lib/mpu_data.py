class GyroData:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return "GyroData({}, {}, {})".format(self.x, self.y, self.z)


class AccelData:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return "AccelData({}, {}, {})".format(self.x, self.y, self.z)


class Mpu6050Data:
    def __init__(self, gyro, accel):
        self.gyro = gyro
        self.accel = accel

    def __str__(self):
        return " - ".join([str(self.gyro), str(self.accel)])

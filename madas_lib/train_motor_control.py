# -*- coding: utf-8 -*-
from RPi import GPIO


class MaxSpeedExceeded(Exception):
    pass


class AbstractTrainMotorControl:
    def __init__(self, bridge_pin, direction_pin, pwm_pin, max_speed_perc):
        """Initialize the train's motor controller. Arguments need to provide
        the corresponding pin number (usually: 16, 18, 22 for LRT v2 PCB) and
        the maximum allowed speed (settings higher than this will cause an
        Exception to be raised, usually 50 for LRT standard track)."""
        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)  # in case it is already in use

        GPIO.setup(bridge_pin,    GPIO.OUT)
        GPIO.setup(direction_pin, GPIO.OUT)
        GPIO.setup(pwm_pin,       GPIO.OUT)

        GPIO.output(bridge_pin,    GPIO.LOW)
        GPIO.output(direction_pin, GPIO.HIGH)

        self.__motor_pwm = GPIO.PWM(pwm_pin, 2000)
        self.__motor_pwm.start(0)

        self.__direction_pin = direction_pin

        self.__max_speed_perc = max_speed_perc

    def set_speed(self, speed_perc):
        """Change speed_perc percentage of maximum speed and go forward if
        forward is True, else backwards.
        """
        if not -100 <= speed_perc <= 100:
            raise ValueError()
        if abs(speed_perc) > self.__max_speed_perc:
            raise MaxSpeedExceeded()

        GPIO.output(self.__direction_pin, GPIO.HIGH if speed_perc >= 0 else GPIO.LOW)
        self.__motor_pwm.ChangeDutyCycle(abs(speed_perc))

    def shutdown(self):
        self.set_speed(0)
        self.__motor_pwm.stop()


class TrainMotorControl(AbstractTrainMotorControl):
    BRIDGE_PIN     = 16
    DIRECTION_PIN  = 18
    PWM_PIN        = 22
    MAX_SPEED_PERC = 50

    def __init__(self):
        super().__init__(self.BRIDGE_PIN,
                         self.DIRECTION_PIN,
                         self.PWM_PIN,
                         self.MAX_SPEED_PERC)

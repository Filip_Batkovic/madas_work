import gevent

from madas_lib.mpu_data import Mpu6050Data, GyroData, AccelData
from madas_lib.publisher import Publisher
import madas_lib.endpoints as endpoints
import madas_lib.thirdparty.mpu6050 as mpu6050


class MpuPublisher(Publisher):
    def __init__(self):
        super().__init__(self._get_new_data, endpoints.mpu6050_publisher_endpoint(), .1)
        self.__mpu6050 = mpu6050.mpu6050(0x69)

    def _get_new_data(self):
        return self.__data_from_raw(self.__mpu6050.get_gyro_data(), self.__mpu6050.get_accel_data())

    @classmethod
    def __data_from_raw(cls, g, a):
        return Mpu6050Data(
                GyroData(g['x'], g['y'], g['z']),
                AccelData(a['x'], a['y'], a['z']))


if __name__=="__main__":
    publisher = MpuPublisher()
    gevent.joinall(list(publisher.to_join()))

import zmq.green as zmq
import gevent
import atexit

from madas_lib.exceptions import ConnectionLost
from madas_lib.joinable import Joinable


class ZmqBidirSocketBase:
    def __init__(self, endpoint, timeout, initial_timeout, socket_type):
        """Create a Client or Server that will connect (Client) or bind
        (Server) to the given endpoint. Generally, timeout sets the time
        allowed for the other end to send either a response (Requestor) or a
        new request (Server). In other words, if, after calling _socket_recv(),
        no data is received within timeout seconds, a ConnectionLost exception
        will be raised. During startup, to the other end to come alive, a
        longer timeout can be used: the *first* call to _socket_recv() will
        wait for initial_timeout seconds before raising the same exception.
        """
        assert socket_type in (zmq.REQ, zmq.REP)

        context = zmq.Context()
        self._socket = context.socket(socket_type)
        if socket_type == zmq.REQ:
            self._socket.connect(endpoint)
        else:
            self._socket.bind(endpoint)

        self._endpoint = endpoint
        self.__timeout = timeout
        self.__initial_timeout = initial_timeout
        self.__first = True

        self.__poller = zmq.Poller()
        self.__poller.register(self._socket, zmq.POLLIN)

    def _socket_recv(self):
        """See description in __init__."""
        to = self.__initial_timeout if self.__first else self.__timeout
        self.__first = False

        socks = dict(self.__poller.poll(to * 1000))
        if self._socket in socks and socks[self._socket] == zmq.POLLIN:
            return self._socket.recv_pyobj()
        else:
            self._socket.setsockopt(zmq.LINGER, 0)
            self._socket.close()
            self.__poller.unregister(self._socket)
            raise ConnectionLost(self._endpoint)


class RawClient(ZmqBidirSocketBase):
    """Implements the Client side of a Client-Server pair. This is the
    initiator part that will send a request and wait for a reply."""
    def __init__(self, endpoint, timeout, initial_timeout):
        super().__init__(endpoint, timeout, initial_timeout, zmq.REQ)

    def request(self, msg):
        self._socket.send_pyobj(msg)
        return self._socket_recv()


class RawServer(ZmqBidirSocketBase):
    """Implements the Server side of a Client-Server pair. This is the passive
    part that, once reply() is called, will block until data is available (or a
    timeout occurred), pass the received data to callback, and reply with
    callback's return value."""
    def __init__(self, endpoint, timeout, initial_timeout, callback):
        super().__init__(endpoint, timeout, initial_timeout, zmq.REP)
        self.__callback = callback

    def reply(self):
        recvd = self._socket_recv()
        self._socket.send_pyobj(self.__callback(recvd))


class Server(RawServer, Joinable):
    """Implements an active Server that include the "endless" loop around
    RawServer.reply(). set_func will be called with any new request that is
    received, and its return value will be sent back to the client. If the
    Server's execution ends for whatever reason (ie., because of a shutdown or
    an exception), shutdown_func is called.
    """
    def __init__(self, endpoint, timeout, initial_timeout, set_func, shutdown_func=lambda: None):
        RawServer.__init__(self, endpoint, timeout, initial_timeout, set_func)
        Joinable.__init__(self)
        self._register_to_join(gevent.spawn(self.__serve))
        self.__shutdown_func = shutdown_func
        # Register the shutdown function to be executed when the interpreter
        # dies; we need this since an exception (eg. KeyboardInterrupt!) is
        # only raised within the currently active greenlet, which will most
        # probably not be __serve, since the socket's recv function (which is
        # indirectly called by self.reply) will not block if we're using
        # gevent. We still keep the finally in __serve around, however, just to
        # show we didn't forget it :). We will never need it, since, even in
        # the unlikely event the KeyboardInterrupt is actually received in
        # __serve, the atexit would still catch it.
        atexit.register(self.__shutdown_func)

    def __serve(self):
        # see the call to atexit.register() above, the finally block is not
        # actually needed!
        try:
            while not self._shutdown_requested():
                self.reply()
        finally:
            self.__shutdown_func()

import gevent

from madas_lib.train_motor_control import TrainMotorControl
from madas_lib.bidir_connection import Server
from madas_lib import endpoints


class TrainMotorControlServer(Server):
    def __init__(self):
        self.tmc = TrainMotorControl()
        super().__init__(endpoints.train_motor_control_server_endpoint(), 1, 10, self.tmc.set_speed, self.tmc.shutdown)


if __name__=="__main__":
    s = TrainMotorControlServer()
    gevent.joinall(list(s.to_join()))

def mpu6050_publisher_endpoint():
    return "tcp://*:5556"


def mpu6050_subscriber_endpoint(remote):
    return mpu6050_publisher_endpoint().replace('*', remote)


def train_motor_control_server_endpoint():
    return "tcp://*:5555"


def train_motor_control_client_endpoint(remote):
    return train_motor_control_server_endpoint().replace('*', remote)

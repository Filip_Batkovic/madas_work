PYTHON=python3

.PHONY: tests

tests:
	nosetests3

mpu_plot:
	$(PYTHON) -m examples.mpu_plot_example && echo "Client ended" &
	$(PYTHON) -m madas_lib.mpu_publisher && echo "Server ended"

train_motor_example_local:
	$(PYTHON) -m examples.train_motor_control_example

train_motor_example:
	$(PYTHON) -m examples.train_motor_control_client_example &
	$(PYTHON) -m madas_lib.train_motor_control_server

derail_detection_controller_example:
	$(PYTHON) -m examples.derail_detection_controller &
	$(PYTHON) -m madas_lib.train_motor_control_server &
	$(PYTHON) -m madas_lib.mpu_publisher
